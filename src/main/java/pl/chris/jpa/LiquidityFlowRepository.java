package pl.chris.jpa;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import pl.chris.entity.LiquidityFlowEntity;

@Repository
public interface LiquidityFlowRepository extends JpaRepository<LiquidityFlowEntity, UUID>, JpaSpecificationExecutor<LiquidityFlowEntity> {
}
