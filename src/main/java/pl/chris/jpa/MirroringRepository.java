package pl.chris.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.chris.entity.MirroringEntity;
@Repository
public interface MirroringRepository extends JpaRepository<MirroringEntity, Long> {
}
