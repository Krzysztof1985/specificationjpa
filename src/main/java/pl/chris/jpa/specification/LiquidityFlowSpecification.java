package pl.chris.jpa.specification;

import java.util.UUID;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

import org.springframework.data.jpa.domain.Specification;


import pl.chris.entity.LiquidityFlowEntity;
import pl.chris.entity.LiquidityFlowEntity_;

public class LiquidityFlowSpecification {


    public static Specification<LiquidityFlowEntity> findParentChild(UUID uuid) {
        return (root, query, builder) -> {
            Predicate uuidPredicate = builder.equal(root.get(LiquidityFlowEntity_.ID), uuid);

            Path<LiquidityFlowEntity> childPathFlowPath = root.get(LiquidityFlowEntity_.CHILD_FLOW);

            Path<LiquidityFlowEntity> parentPath = root.get(LiquidityFlowEntity_.PARENT_FLOW);
//            Predicate parentNullablePredicate = builder.isNull(parentPath);

            Predicate childFlowPredicate = builder.isNotNull(childPathFlowPath);

            Predicate parentFlowPredicateQuery = builder.and(uuidPredicate, childFlowPredicate);

            Predicate parentExistsPredicate = builder.isNotNull(parentPath);

            Predicate childUuidPredicate = builder.equal(childPathFlowPath
                    .get("id"), uuid);

            Predicate childFlowPredicateQuery = builder.and(childUuidPredicate, parentExistsPredicate);

            return builder.or(parentFlowPredicateQuery, childFlowPredicateQuery);
        };
    }
}
