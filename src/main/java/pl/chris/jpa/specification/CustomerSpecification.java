package pl.chris.jpa.specification;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import javax.persistence.criteria.Predicate;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.jpa.domain.Specification;

import pl.chris.entity.CustomerEntity;
import pl.chris.entity.CustomerEntity_;
import pl.chris.entity.ItemEntity_;
import pl.chris.filter.CustomerFilter;

public class CustomerSpecification {

    public static Specification<CustomerEntity> filter(CustomerFilter filter) {
        return (root, query, builder) -> {

            Predicate fillFilterPredicate = Stream.of(
                    Optional.ofNullable(filter.getAge())
                            .map(age -> builder.equal(root.get(CustomerEntity_.AGE), age))
                            .orElse(null),

                    Optional.ofNullable(filter.getIds())
                            .filter(CollectionUtils::isNotEmpty)
                            .map(coll -> root.get(CustomerEntity_.CUSTOMER_ID)
                                    .in(coll))
                            .orElse(null),

                    /**
                     * equals to exact value
                     */
                    Optional.ofNullable(filter.getName())
                            .map(name ->
//                                    builder.like(builder.lower(root.get(CustomerEntity_.NAME)), name.toLowerCase())
                                    builder.equal(builder.lower(root.get(CustomerEntity_.NAME)), name.toLowerCase())
                            )

                            .orElse(null),

                    /**
                     * equals to like %value%
                     */
//                    Optional.ofNullable(filter.getName())
//                            .map(name -> builder.like(root.get(CustomerEntity_.NAME), "%" + name + "%"))
//                            .orElse(null),

                    Optional.ofNullable(filter.getLocations())
                            .filter(CollectionUtils::isNotEmpty)
                            .map(locations -> root.get(CustomerEntity_.LOCATION)
                                    .in(locations))
                            .orElse(null),

                    Optional.ofNullable(filter.getItemNames())
                            .filter(CollectionUtils::isNotEmpty)
                            .map(itemNames ->
                                    root.join(CustomerEntity_.ITEMS)
                                            .get(ItemEntity_.ITEM_NAME)
                                            .in(itemNames)
                            )
                            .orElse(null)
            )
                    .filter(Objects::nonNull)
                    .reduce(builder.conjunction(), builder::and);

            return fillFilterPredicate;
        };
    }
}
