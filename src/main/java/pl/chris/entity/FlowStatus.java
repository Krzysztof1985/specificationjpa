package pl.chris.entity;

public enum FlowStatus {
    CONFIRMED,
    PREDICTION,
    ACTUAL,
    INTRADAY
}
