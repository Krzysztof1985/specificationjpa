package pl.chris.entity;

public enum FlowType {
    PARENT,
    CHILD
}
