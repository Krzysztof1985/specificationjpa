package pl.chris.entity;

public enum MirrorDirection {
    NO,
    FROM_DEBITS,
    FROM_CREDITS
}
