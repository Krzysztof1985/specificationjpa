package pl.chris.entity;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Table(name = "LIQUIDITY_FLOW")
@Entity
public class LiquidityFlowEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @Column
    private BigDecimal amount;

    @Column
    private String name;
    @Enumerated(EnumType.STRING)
    private FlowStatus flowStatus;

    @OneToOne(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            orphanRemoval = true)
    private LiquidityFlowEntity childFlow;

    @OneToOne(mappedBy = "childFlow")
    private LiquidityFlowEntity parentFlow;

    @Transient
    public FlowType getFlowType() {
        if (Objects.nonNull(this.getChildFlow()) || Objects.isNull(this.parentFlow)) {
            return FlowType.PARENT;
        }
        else {
            return FlowType.CHILD;
        }
    }

    @Column
    private String updatedBy;
}
