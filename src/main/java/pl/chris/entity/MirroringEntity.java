package pl.chris.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@Table(name = "mirroring")
@Entity
public class MirroringEntity {
    @Id
    private Long userId;
    @Column
    @Enumerated(EnumType.STRING)
    private MirrorDirection direction;

    @Transient
    public MirroringEntity deepClone() {
        return MirroringEntity.of(this.userId, this.direction);
    }
}
