package pl.chris.exception;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Order(HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class ExceptionAdvice extends ResponseEntityExceptionHandler {

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = {IllegalStateException.class})
    public ResponseWrapper<Void> handleIllegalStateException(RuntimeException ex, WebRequest request) {
        log.error("Issue for request url {}", request.getContextPath());
        return ResponseWrapper.notFound()
                .rootMessage(ResponseWrapper.Message.of(HttpStatus.INTERNAL_SERVER_ERROR.name(), "entity.not.found", ex.getMessage()))
                .build();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = {IllegalArgumentException.class})
    public ResponseWrapper<Void> handleIllegalArgumentException(RuntimeException ex, WebRequest request) {
        log.error("Issue for request url {}", request.getContextPath());
        return ResponseWrapper.notFound()
                .rootMessage(ResponseWrapper.Message.of(HttpStatus.INTERNAL_SERVER_ERROR.name(), "Provided unsupported value", ex.getMessage()))
                .build();
    }
}
