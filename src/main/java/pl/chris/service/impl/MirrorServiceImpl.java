package pl.chris.service.impl;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import pl.chris.api.MirroringSetupService;
import pl.chris.dto.flow.LiquidityFlowDto;
import pl.chris.entity.LiquidityFlowEntity;
import pl.chris.jpa.MirroringRepository;
import pl.chris.service.MirroringService;
import pl.chris.service.impl.factories.MirroringFlowStrategy;
import pl.chris.service.impl.factories.MirrorFlowFactory;
import pl.chris.util.OperationType;

@Service
@RequiredArgsConstructor
public class MirrorServiceImpl implements MirroringService {

    private static final Long DUMMY_USER_ID = 1L;
    private final MirrorFlowFactory mirrorFlowFactory = new MirrorFlowFactory();

    private final MirroringSetupService mirroringSetupService;
    private final MirroringRepository mirroringRepository;

    @Override
    public void performMirroringOnCreate(LiquidityFlowDto liquidityFlowDto, LiquidityFlowEntity entity) {
        //TODO: tomorrow
        //generate mirror flow producer (with copy)

        MirroringFlowStrategy flowStrategy = mirrorFlowFactory.getFlowStrategy(entity.getFlowType(), OperationType.CREATE);
        flowStrategy.performMirroring(liquidityFlowDto, entity);
    }

    @Override
    public void performMirroringOnUpdate(LiquidityFlowDto liquidityFlowDto, LiquidityFlowEntity entity) {
        MirroringFlowStrategy flowStrategy = mirrorFlowFactory.getFlowStrategy(entity.getFlowType(), OperationType.UPDATE);
        flowStrategy.performMirroring(liquidityFlowDto, entity);
    }
}
