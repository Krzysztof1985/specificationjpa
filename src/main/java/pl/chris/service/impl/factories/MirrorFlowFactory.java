package pl.chris.service.impl.factories;

import pl.chris.entity.FlowType;
import pl.chris.service.impl.factories.create.CreateMirrorFlowProducer;
import pl.chris.service.impl.factories.update.UpdateMirrorFlowProducer;
import pl.chris.util.OperationType;

public class MirrorFlowFactory extends AbstractMirroFlowFactory {

    @Override
    public MirroringFlowStrategy getFlowStrategy(FlowType flowType, OperationType operationType) {
        switch (operationType) {
            case CREATE:
                return CreateMirrorFlowProducer.getMirrorStretegyForCreation(flowType);
            case UPDATE:
                return UpdateMirrorFlowProducer.getStrategyForUpdate(flowType);
            default:
                throw new IllegalStateException("Provided unsupported operration type "+operationType);
        }
    }
}
