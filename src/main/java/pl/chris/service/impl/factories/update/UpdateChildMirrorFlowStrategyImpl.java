package pl.chris.service.impl.factories.update;

import pl.chris.dto.flow.LiquidityFlowDto;
import pl.chris.entity.LiquidityFlowEntity;
import pl.chris.mapper.EnumConstantMapper;

public class UpdateChildMirrorFlowStrategyImpl implements UpdateMirrorFlowStrategy {


    EnumConstantMapper enumConstantMapper = EnumConstantMapper.INSTANCE;
    @Override
    public void performMirroring(LiquidityFlowDto dto, LiquidityFlowEntity entity) {
        dto.setAmount(entity.getAmount());
        dto.setFlowType(enumConstantMapper.mapFlowStatusToString(entity.getFlowStatus()));
    }
}
