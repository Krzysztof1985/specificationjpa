package pl.chris.service.impl.factories;


import pl.chris.entity.FlowType;
import pl.chris.util.OperationType;

abstract class AbstractMirroFlowFactory {
    public abstract MirroringFlowStrategy getFlowStrategy(FlowType flowType, OperationType operationType);
}
