package pl.chris.service.impl.factories.update;

import pl.chris.service.impl.factories.MirroringFlowStrategy;

public interface UpdateMirrorFlowStrategy extends MirroringFlowStrategy {
}
