package pl.chris.service.impl.factories;

import pl.chris.dto.flow.LiquidityFlowDto;
import pl.chris.entity.LiquidityFlowEntity;

public interface MirroringFlowStrategy {

    void performMirroring(LiquidityFlowDto dto, LiquidityFlowEntity entity);
}
