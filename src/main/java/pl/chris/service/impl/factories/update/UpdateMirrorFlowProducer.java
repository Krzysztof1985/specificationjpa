package pl.chris.service.impl.factories.update;

import pl.chris.entity.FlowType;
import pl.chris.service.impl.factories.MirroringFlowStrategy;
import pl.chris.service.impl.factories.create.CreateMirrorParentFlowtStrategyImpl;

public final class UpdateMirrorFlowProducer {
    private UpdateMirrorFlowProducer() {
    }

    public static MirroringFlowStrategy getStrategyForUpdate(FlowType flowType) {
        switch (flowType) {
            case PARENT:
                return new CreateMirrorParentFlowtStrategyImpl();
            case CHILD:
                return new UpdateChildMirrorFlowStrategyImpl();
            default:
                throw new IllegalStateException("Provided unsupported mirror flow type " + flowType);
        }
    }
}

