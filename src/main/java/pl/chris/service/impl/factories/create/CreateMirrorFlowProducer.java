package pl.chris.service.impl.factories.create;

import javax.validation.constraints.NotNull;

import pl.chris.entity.FlowType;
import pl.chris.service.impl.factories.MirroringFlowStrategy;

public final class CreateMirrorFlowProducer {

    private CreateMirrorFlowProducer() {
    }

    public static MirroringFlowStrategy getMirrorStretegyForCreation(@NotNull FlowType flowType) {
        switch (flowType) {
            case PARENT:
            case CHILD:
                return new CreateMirrorParentFlowtStrategyImpl();
            default:
                throw new IllegalStateException("Provided unsupported type " + flowType);

        }
    }
}
