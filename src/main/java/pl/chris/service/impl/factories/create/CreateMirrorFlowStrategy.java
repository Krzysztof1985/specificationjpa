package pl.chris.service.impl.factories.create;

import pl.chris.service.impl.factories.MirroringFlowStrategy;

public interface CreateMirrorFlowStrategy extends MirroringFlowStrategy {
}
