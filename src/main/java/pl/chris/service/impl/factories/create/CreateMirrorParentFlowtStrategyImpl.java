package pl.chris.service.impl.factories.create;

import pl.chris.dto.flow.LiquidityFlowDto;
import pl.chris.entity.FlowStatus;
import pl.chris.entity.LiquidityFlowEntity;
import pl.chris.mapper.EnumConstantMapper;

public class CreateMirrorParentFlowtStrategyImpl implements CreateMirrorFlowStrategy {

    EnumConstantMapper enumConstantMapper = EnumConstantMapper.INSTANCE;

    @Override
    public void performMirroring(LiquidityFlowDto dto, LiquidityFlowEntity entity) {
        FlowStatus flowStatus = enumConstantMapper.mapStringToFlowStatus(dto.getFlowType());
        entity.setFlowStatus(flowStatus);
        entity.setName(dto.getName());
        entity.setAmount(dto.getAmount());
        entity.setName(dto.getName());
        entity.setUpdatedBy(dto.getUpdatedBy());
    }
}
