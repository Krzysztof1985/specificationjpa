package pl.chris.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.chris.api.CustomerService;
import pl.chris.dto.customer.CustomerCreateUpdateDto;
import pl.chris.dto.customer.CustomerDto;
import pl.chris.entity.CustomerEntity;
import pl.chris.filter.CustomerFilter;
import pl.chris.jpa.CustomerRepository;
import pl.chris.jpa.specification.CustomerSpecification;
import pl.chris.mapper.CustomerMapper;

@RestController
@RequestMapping("/customer")
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    private final CustomerMapper mapper = CustomerMapper.INSTANCE;

    @Override
    public List<CustomerDto> findAll(@Valid CustomerFilter filter) {
        List<CustomerEntity> all = customerRepository.findAll(CustomerSpecification.filter(filter));
        List<CustomerDto> result = new ArrayList<>();
        all.forEach(item -> result.add(mapper.convertToDto(item)));
        return result;
    }

    @Override
    public int count(@Valid CustomerFilter filter) {
        Long count = customerRepository.count(CustomerSpecification.filter(filter));
        return count.intValue();
    }

    @Override
    public CustomerDto findById(@NotNull Long id) {
        CustomerEntity customerEntity =
                Optional.ofNullable(customerRepository.findById(id))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .orElseThrow(() -> new IllegalStateException("Entity not found!"));
        return mapper.convertToDto(customerEntity);
    }

    @Override
    public void update(@NotNull Long id, @NotNull CustomerCreateUpdateDto customer) {
        customerRepository.findById(id)
                .ifPresentOrElse(entity -> {
                    CustomerEntity customerEntity = mapper.convertToCreateUpdateEntity(customer);
                    customerEntity.setCustomerId(entity.getCustomerId());
                    customerRepository.save(customerEntity);
                }, () -> new IllegalStateException("Unable to update entity with id" + id));
    }

    @Override
    public long createCustomer(@NotNull CustomerCreateUpdateDto customer) {
        CustomerEntity entity = mapper.convertToCreateUpdateEntity(customer);
        CustomerEntity savedEntity = customerRepository.save(entity);
        CustomerDto dto = mapper.convertToDto(savedEntity);
        return dto.getId();
    }
}
