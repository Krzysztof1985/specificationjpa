package pl.chris.service;

import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.web.bind.annotation.RestController;

import pl.chris.api.HelloService;

@RestController
public class HelloServiceImpl implements HelloService {

    public String getAuthor() {
        return "Unknown!";
    }

    public LocalDate getDate() {
        return LocalDate.now();
    }

    public LocalTime getTime() {
        return LocalTime.now();
    }

    public String hello() {
        return "YELLOW FROM SPRING";
    }
}
