package pl.chris.service;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import pl.chris.api.MirroringSetupService;
import pl.chris.dto.mirroring.CreateMirroringRequestDto;
import pl.chris.dto.mirroring.MirroringDto;
import pl.chris.entity.MirroringEntity;
import pl.chris.jpa.MirroringRepository;
import pl.chris.mapper.MirroringMapper;

@RestController
@RequiredArgsConstructor
@RequestMapping("/mirroring")
public class MirroringSetupServiceImpl implements MirroringSetupService {

    private final MirroringRepository repository;

    private final MirroringMapper mirroringMapper = MirroringMapper.INSTANCE;

    @Override
    public void createMirroring(CreateMirroringRequestDto requestDto) {
        MirroringEntity mirroringEntity = mirroringMapper.mapCreateRequestToEntity(requestDto);
        repository.save(mirroringEntity);
    }

    @Override
    public List<MirroringDto> findAllMirrorings() {
        List<MirroringEntity> entities = repository.findAll();
        return mirroringMapper.mapEntityToMirroringDtoCollection(entities);
    }

    @Override
    public MirroringDto findMirroringByUserId(Long id) {
        return repository.findById(id)
                .map(mirroringMapper::mapEntityToMirroringDto)
                .orElseThrow(() -> new IllegalStateException("Unable to find user with ID " + id));
    }
}
