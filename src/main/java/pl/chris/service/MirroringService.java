package pl.chris.service;

import pl.chris.dto.flow.LiquidityFlowDto;
import pl.chris.entity.LiquidityFlowEntity;

public interface MirroringService {
    void performMirroringOnCreate(LiquidityFlowDto liquidityFlowDto, LiquidityFlowEntity entity);
    void performMirroringOnUpdate(LiquidityFlowDto liquidityFlowDto, LiquidityFlowEntity entity);
}
