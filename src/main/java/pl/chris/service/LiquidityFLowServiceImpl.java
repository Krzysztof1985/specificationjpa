package pl.chris.service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import pl.chris.api.LiquidityFlowService;
import pl.chris.dto.flow.CreateLiquidityFlowDto;
import pl.chris.dto.flow.LiquidityFlowDto;
import pl.chris.dto.customer.MirrorFlowResponse;
import pl.chris.dto.flow.UpdateLiquidityFlowDto;
import pl.chris.entity.LiquidityFlowEntity;
import pl.chris.jpa.LiquidityFlowRepository;
import pl.chris.jpa.specification.LiquidityFlowSpecification;
import pl.chris.mapper.LiqudityFlowMapper;

@RestController
@RequiredArgsConstructor
@RequestMapping("/flows")
public class LiquidityFLowServiceImpl implements LiquidityFlowService {

    private final LiqudityFlowMapper mapper = LiqudityFlowMapper.INSTANCE;
    private final LiquidityFlowRepository liquidityFlowRepository;

    private final MirroringService mirroringService;

    @Override
    public List<LiquidityFlowDto> findAll() {
        List<LiquidityFlowDto> result = liquidityFlowRepository.findAll()
                .stream()
                .map(mapper::mapLiquidtyFlowToDto)
                .collect(Collectors.toList());
        return result;
    }

    @Override
    public LiquidityFlowDto createLiquidityFlow(CreateLiquidityFlowDto createDto) {
        LiquidityFlowEntity entity = mapper.mapCreateDtoToLiquidityFlowEntity(createDto);

        LiquidityFlowEntity savedEntity = liquidityFlowRepository.save(entity);
        return mapper.mapLiquidtyFlowToDto(savedEntity);
    }

    @Override
    public MirrorFlowResponse findAll(UUID id) {
        LiquidityFlowEntity liquidityFlowEntity = liquidityFlowRepository.findOne(LiquidityFlowSpecification.findParentChild(id))
                .orElseThrow(() -> new IllegalStateException("Unable to find mirror flows by uuid " + id));
        LiquidityFlowDto parent = mapper.mapLiquidtyFlowToDto(liquidityFlowEntity);
        LiquidityFlowDto child = mapper.mapLiquidtyFlowToDto(liquidityFlowEntity.getChildFlow());

        return MirrorFlowResponse.of(parent, child);
    }

    @Override
    public LiquidityFlowDto updateLiquidityFlow(UUID uuid, UpdateLiquidityFlowDto dto) {

        LiquidityFlowEntity liquidityFlowEntity = liquidityFlowRepository.findById(uuid)
                .orElseThrow(() -> new IllegalStateException("Unable to find record with uuid " + uuid));


        return null;
    }

}