package pl.chris.dto.flow;

import java.math.BigDecimal;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateLiquidityFlowDto {
    private String name;
    @Schema(required = true,
            allowableValues = {"CONFIRMED", "PREDICTION", "ACTUAL", "INTRADAY"})
    private String flowStatus;

    private String flowType;

    private BigDecimal amount;

    private String updatedBy;


}
