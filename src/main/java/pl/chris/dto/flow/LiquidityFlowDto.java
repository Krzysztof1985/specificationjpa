package pl.chris.dto.flow;

import java.math.BigDecimal;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LiquidityFlowDto {
    private UUID id;

    private String name;
    @Schema(required = true, allowableValues = {"CONFIRMED","PREDICTION","ACTUAL", "INTRADAY"})
    private String flowStatus;

    private String flowType;

    private BigDecimal amount;

    private String referencedFlowUuid;

    private String updatedBy;
}
