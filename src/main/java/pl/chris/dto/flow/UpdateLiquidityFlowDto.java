package pl.chris.dto.flow;

import java.math.BigDecimal;

import io.swagger.v3.oas.annotations.media.Schema;

public class UpdateLiquidityFlowDto {

    private String name;
    @Schema(required = true,
            allowableValues = {"CONFIRMED", "PREDICTION", "ACTUAL", "INTRADAY"})
    private String flowStatus;

    private BigDecimal amount;

    private String updatedBy;

}
