package pl.chris.dto.customer;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerCreateUpdateDto {
    @NotNull
    @Size(min = 3, max = 50)
    @Schema(name = "name",
            type = "string",
            required = true,
            example = "Alice")
    private String name;

    @NotNull
    @Size(min = 3, max = 50)
    @Schema(name = "location",
            type = "string",
            required = true,
            example = "Poland")
    private String location;

    @Positive
    @Schema(name = "age",
            type = "integer",
            required = true,
            example = "30")
    private Integer age;
}
