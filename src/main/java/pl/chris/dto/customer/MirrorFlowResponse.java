package pl.chris.dto.customer;

import lombok.Value;
import pl.chris.dto.flow.LiquidityFlowDto;

@Value(staticConstructor = "of")
public class MirrorFlowResponse {
 LiquidityFlowDto parent;
 LiquidityFlowDto child;


}
