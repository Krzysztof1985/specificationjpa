package pl.chris.dto.customer;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {

    @Schema(name = "id",
            type = "long",
            required = true,
            example = "1")
    private Long id;

    @Schema(name = "name",
            type = "string",
            required = true,
            example = "Alice")
    private String name;

    @Schema(name = "location",
            type = "string",
            required = true,
            example = "Poland")
    private String location;

    @Schema(name = "age",
            type = "integer",
            required = true,
            example = "30")
    private Integer age;
}
