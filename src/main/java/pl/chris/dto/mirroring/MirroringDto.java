package pl.chris.dto.mirroring;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;
@Builder
@Value(staticConstructor = "of")
public class MirroringDto {
    @Schema(name = "User id",
            required = true, example = "1")
    private Long userId;

    @Schema(name = "Mirroring type",
            required = true,
            allowableValues = {"NO", "FROM_DEBITS", "FROM_CREDITS"}, example = "FROM_DEBITS")
    private String mirroringType;
}
