package pl.chris.api;

import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.web.bind.annotation.GetMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = HelloService.TAG)
public interface HelloService {

    String TAG = "Hello Service";

    @Operation(tags = TAG,
            summary = "Says hello")
    @GetMapping("/hello")
    public String hello();

    @GetMapping("/getAuthor")
    @Operation(tags = TAG,
            summary = "This method is used to get the author name.")
    public String getAuthor();

    @Operation(tags = TAG,
            summary = "This method is used to get the current date.")
    @GetMapping("/getDate")
    public LocalDate getDate();

    @Operation(tags = TAG,
            summary = "This method is used to get the current time.")
    @GetMapping("/getTime")
    public LocalTime getTime();
}
