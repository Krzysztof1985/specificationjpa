package pl.chris.api;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import pl.chris.dto.mirroring.CreateMirroringRequestDto;
import pl.chris.dto.mirroring.MirroringDto;

@Tag(name = MirroringSetupService.TAG)
public interface MirroringSetupService {
    String TAG = "Mirroring Setup Service";

    @Operation(tags = TAG,
            summary = "Creates mirroring for given user")
    @PostMapping
    void createMirroring(@Valid @RequestBody CreateMirroringRequestDto requestDto);

    @Operation(tags = TAG,
            summary = "Gets all mirroring details for all users")
    @GetMapping
    List<MirroringDto> findAllMirrorings();


    @Operation(tags = TAG,
            summary = "Gets mirroring details for specific user")
    @GetMapping("/{id}")
    MirroringDto findMirroringByUserId(@PathVariable @NotNull Long id);
}
