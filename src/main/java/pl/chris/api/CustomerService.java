package pl.chris.api;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import pl.chris.dto.customer.CustomerDto;
import pl.chris.dto.customer.CustomerCreateUpdateDto;
import pl.chris.filter.CustomerFilter;


@Tag(name = CustomerService.TAG)
public interface CustomerService {

    String TAG = "Customer Service";

    @Operation(tags = TAG,
            summary = "Find all customers by a filter")
    @PostMapping("/by-filter")
    List<CustomerDto> findAll(@Valid @RequestBody CustomerFilter filter);

    @Operation(tags = TAG,
            summary = "Find the count of customers by specific criteria")
    @PostMapping("/count-by-filter")
    int count(@Valid @RequestBody CustomerFilter filter);

    @Operation(tags = TAG,
            summary = "Gets customer based on id")
    @GetMapping("/{id:\\d+}")
    CustomerDto findById(@PathVariable("id") @NotNull Long id);

    @Operation(tags = TAG,
            summary = "Updates given customer")
    @PutMapping("/{id:\\d+}")
    void update(@Parameter(description = "The id of the customer, which will be updated") @PathVariable("id") @NotNull Long id,
            @NotNull @RequestBody CustomerCreateUpdateDto customer);

    @Operation(tags = TAG, summary = "Creates a customer")
    @PostMapping("/")
    long createCustomer(@NotNull @RequestBody CustomerCreateUpdateDto customer);
}
