package pl.chris.api;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import pl.chris.dto.flow.CreateLiquidityFlowDto;
import pl.chris.dto.flow.LiquidityFlowDto;
import pl.chris.dto.customer.MirrorFlowResponse;
import pl.chris.dto.flow.UpdateLiquidityFlowDto;

@Tag(name = LiquidityFlowService.TAG)
public interface LiquidityFlowService {

    String TAG = "Liquidity Flow Service";

    @Operation(tags = TAG,
            summary = "Find all liqudity flows")
    @GetMapping
    List<LiquidityFlowDto> findAll();

    @Operation(tags = TAG, summary = "Create liquidty flow")
    LiquidityFlowDto createLiquidityFlow(@Valid @RequestBody CreateLiquidityFlowDto createDto);

    @Operation(tags = TAG,
            summary = "Find all mirrored liqudity flows")
    @GetMapping("/{id}")
    MirrorFlowResponse findAll(@PathVariable("id") @NotNull UUID uuid);
    @Operation(tags = TAG,
            summary = "Updates liquidity flow")
    @PutMapping("/{id}")
    LiquidityFlowDto updateLiquidityFlow(@PathVariable("id") @NotNull UUID uuid, @Valid @RequestBody UpdateLiquidityFlowDto dto);
}
