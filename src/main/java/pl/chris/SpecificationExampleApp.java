package pl.chris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpecificationExampleApp {

    public static void main(String[] args) {
        SpringApplication.run(SpecificationExampleApp.class, args);
    }

}
