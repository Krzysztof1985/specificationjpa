package pl.chris.mapper;


import java.util.Optional;
import java.util.UUID;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import pl.chris.dto.flow.CreateLiquidityFlowDto;
import pl.chris.dto.flow.LiquidityFlowDto;
import pl.chris.entity.FlowType;
import pl.chris.entity.LiquidityFlowEntity;

@Mapper
public interface LiqudityFlowMapper {
    LiqudityFlowMapper INSTANCE = Mappers.getMapper(LiqudityFlowMapper.class);

    @Mappings(
            {
                    @Mapping(target = "childFlow",
                            ignore = true),
                    @Mapping(target = "parentFlow",
                            ignore = true)
            }
    )
    LiquidityFlowEntity mapToLiquidityFlowEntity(LiquidityFlowDto input);

    @Mappings(
            {
                    @Mapping(target = "id",
                            ignore = true),
                    @Mapping(target = "childFlow",
                            ignore = true),
                    @Mapping(target = "parentFlow",
                            ignore = true)
            }
    )
    LiquidityFlowEntity mapCreateDtoToLiquidityFlowEntity(CreateLiquidityFlowDto input);

    @Mapping(target = "referencedFlowUuid",
            ignore = true)
    LiquidityFlowDto mapLiquidtyFlowToDto(LiquidityFlowEntity input);

    @AfterMapping
    default void setChildFlowUUid(LiquidityFlowEntity input, @MappingTarget LiquidityFlowDto liquidityFlowDto) {
        if (FlowType.PARENT == input.getFlowType()) {
            String refUuid = Optional.ofNullable(input.getChildFlow())
                    .map(LiquidityFlowEntity::getId)
                    .map(UUID::toString)
                    .orElse(null);

            liquidityFlowDto.setFlowType(FlowType.PARENT.name());
            liquidityFlowDto.setReferencedFlowUuid(refUuid);
        }
        else {
            String refUuid = Optional.ofNullable(input.getParentFlow())
                    .map(LiquidityFlowEntity::getId)
                    .map(UUID::toString)
                    .orElse(null);
            liquidityFlowDto.setFlowType(FlowType.CHILD.name());
            liquidityFlowDto.setReferencedFlowUuid(refUuid);
        }

    }
}
