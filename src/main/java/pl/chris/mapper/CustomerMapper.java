package pl.chris.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import pl.chris.dto.customer.CustomerDto;
import pl.chris.dto.customer.CustomerCreateUpdateDto;
import pl.chris.entity.CustomerEntity;

@Mapper
public interface CustomerMapper {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    @Mapping(source = "customerId", target = "id")
    CustomerDto convertToDto(CustomerEntity entity);

    CustomerEntity convertToEntity(CustomerDto dto);

    @Mapping(target = "customerId",
            ignore = true)
    CustomerEntity convertToCreateUpdateEntity(CustomerCreateUpdateDto dto);
}
