package pl.chris.mapper;

import java.util.Collection;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import pl.chris.dto.mirroring.CreateMirroringRequestDto;
import pl.chris.dto.mirroring.MirroringDto;
import pl.chris.entity.MirroringEntity;

@Mapper(uses = EnumConstantMapper.class)
public interface MirroringMapper {

    MirroringMapper INSTANCE = Mappers.getMapper(MirroringMapper.class);

    @Mapping(source = "mirroringType", target = "direction")
    MirroringEntity mapCreateRequestToEntity(CreateMirroringRequestDto input);

    @Mapping(source = "direction", target = "mirroringType")
    MirroringDto mapEntityToMirroringDto(MirroringEntity mirroringEntity);

    List<MirroringDto> mapEntityToMirroringDtoCollection(Collection<MirroringEntity> inputCollection);
}
