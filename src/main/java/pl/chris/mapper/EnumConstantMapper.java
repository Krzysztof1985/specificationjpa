package pl.chris.mapper;

import javax.validation.constraints.NotNull;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import pl.chris.entity.FlowStatus;
import pl.chris.entity.MirrorDirection;

@Mapper
public interface EnumConstantMapper {

    EnumConstantMapper INSTANCE = Mappers.getMapper(EnumConstantMapper.class);

    default MirrorDirection mapToDirection(@NotNull String value) {
        if (value == null) {
            throw new IllegalArgumentException("Unexpected enum constant: null");
        }
        try{
           return MirrorDirection.valueOf(value);
        } catch (Exception exception){
            System.out.println("Provided unsupported type for enum "+ value);
            throw new IllegalArgumentException("Incorrect value "+ value);
        }
    }

  default FlowStatus mapStringToFlowStatus(String flowType){
        if(flowType == null){
            throw new IllegalArgumentException("Flow type cannot be null");
        }
        try{
            return FlowStatus.valueOf(flowType);
        } catch (Exception exception){
            System.out.println("Provided unsupported type for enum "+ flowType);
            throw new IllegalArgumentException("Incorrect value "+ flowType);
        }
    }

    default String mapFlowStatusToString(FlowStatus input){
        if (input == null){
            return null;
        }
        return input.name();
    }

    default String mapDirectionToString(MirrorDirection input){
        if (input == null){
            return null;
        }
        return input.name();
    }
}
