package pl.chris.filter;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerFilter {

    @Schema(name = "ids",
            description = "Set of customer ids",
            example = "[1,2,3]",
            type = "array")
    private Set<Long> ids;

    @Schema(name = "locations",
            description = "Set of locations to filter",
            example = "[\"Poland\", \"Germany\"]",
            type = "array")
    private Set<String> locations;

    @Schema(name = "name",
            description = "user name to find",
            example = "John")
    private String name;

    @Schema(name = "age",
            description = "fetch data based on gte age",
            example = "30",
            type = "integer")
    private Integer age;

    @Schema(name = "itemNames",
            description = "Set of item names to filter",
            example = "[\"Apple\", \"Banana\"]",
            type = "array")
    private Set<String> itemNames;
}
