## Specification JPA API


### I Introduction

This is example Spring boot project which is using REST service to perform CRUD operations.
Main idea is to show that all Jersey client stuff is equal to this sollution.
Additionally it's using Specification JPA API, which is simpler to extend and use when new filter entries will be added.

### II Requirements


| Framework/technology  | Version  |
|:---:|:---:|
|  Java | JDK 11  | 
|  Database | H2  |


On startup application will load some samle data from ```import.sql``` file which is on the classpath in ```src/main/resources```

Rest API endpoints are available on following URL: [http://localhost:8080/swagger-ui/#/](http://localhost:8080/swagger-ui/#/) 	

Access to the database is via web browser using following url [http://localhost:8080/h2-console](http://localhost:8080/h2-console) 
where default user password are:
```yaml
user: admin
password: pass
```
##Enjoy using Specification JPA ;) 
